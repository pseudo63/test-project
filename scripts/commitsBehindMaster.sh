#!/bin/bash

echo "  "
pwd
echo "The current branch is: ${CI_COMMIT_BRANCH}"

echo "  "
echo "Return from api call to master branch of project with id: $CI_PROJECT_ID"
json=$(curl --header "token=$CI_JOB_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/branches/master")

echo "  "
echo "The most recent commit on current branch is: ${CI_COMMIT_SHA}"

# parse commit ID from json: 
BRANCH_COMMIT_ID=$(echo $json | grep -oE '"id":"([^"]*)"')
BRANCH_COMMIT_ID=${BRANCH_COMMIT_ID:6}
BRANCH_COMMIT_ID=${BRANCH_COMMIT_ID%\"}

echo "The most recent commit on remote master is: $BRANCH_COMMIT_ID"
echo "  "

# check for git command failure
git rev-list --count ${BRANCH_COMMIT_ID}..${CI_COMMIT_SHA} > /dev/null 2>&1 || fatal=1

if (( fatal != 1 ))
then
	ahead=$(git rev-list --count ${BRANCH_COMMIT_ID}..${CI_COMMIT_SHA}) > /dev/null 2>&1
	behind=$(git rev-list --count ${CI_COMMIT_SHA}..${BRANCH_COMMIT_ID}) > /dev/null 2>&1
	arrayDiff=($behind $ahead)
else
	echo "The most recent commit on remote master was not found in your branch"
fi

if (( ${#arrayDiff[@]} > 0 ))
then
    echo "Current branch is ${arrayDiff[0]} commits behind master"
    echo "Current branch is ${arrayDiff[1]} commits ahead of master"
	echo "  "
	exit 0
else  
	echo "Ensure that master is correctly merged into ${CI_COMMIT_BRANCH}; exit 1"
	echo "  "
	exit 1
fi


