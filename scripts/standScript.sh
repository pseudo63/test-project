(grep -rn --exclude '*.sh' --exclude '.gitlab-ci.yml' 'TODO\|NOTE\|FIXME\|BUG' $1) || :


if grep -qr --exclude '*.sh' --exclude '.gitlab-ci.yml' 'TODO\|NOTE\|FIXME\|BUG' $1; then
    echo Remaining TODOs, BUGs, NOTEs, or FIXMEs were found
    exit 1
else
    echo No remining TODO entries
    exit 0
fi
