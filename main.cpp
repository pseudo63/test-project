// Telework 6:30 to 8:30 5/24 (Mon)
// Update commit 12

#include <iostream>
#include "echo.h"

int main() {
    Echo e;
    int rowno,colno;
    int counter = 0;
    char next_move;
    while(!e.game_won) {
      std::cout << "Row? ";
      std::cin >> rowno;

      std::cout << "Col? ";
      std::cin >> colno;

      if(counter % 2 == 0) {
        next_move = 'O';
      } else {
        next_move = 'X';
      }

      e.make_move((int) rowno, (int) colno, next_move);
      e.print_board();
      counter++;
      if (counter >= 5) {
        e.game_won = true;
      }
    }
    return 0;
}
