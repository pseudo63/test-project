// pipe-102 is 2 commits ahead and 1 behind

#include <iostream>
#include "echo.h"

void Echo::print_board() {
    std::cout << game_board[0][0] << "  |  " << game_board[1][0] << "  |  " << game_board[2][0] << "\n";
    std::cout << "---------------" << "\n";
    std::cout << game_board[0][1] << "  |  " << game_board[1][1] << "  |  " << game_board[2][1] << "\n";
    std::cout << "---------------" << "\n";
    std::cout << game_board[0][2] << "  |  " << game_board[1][2] << "  |  " << game_board[2][2] << "\n";
}

bool Echo::make_move(int i, int j, char in) {

  game_board[i][j] = in;
  return true;
}
