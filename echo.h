// header file for echo
// adding more comments for git changes 
// ostensibly a change to maste
// pipe-102 is 1 commit ahead
// add in master and in pipe104 twice
#pragma once

class Echo {

public:
  void print_board();
  bool make_move(int x, int y, char in);
  char game_board[3][3] = {' ', ' ',' ',' ',' ',' ',' ',' ',' '};
  bool game_won = false;

};
